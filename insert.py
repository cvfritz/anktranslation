import xlrd
import xlutils
import xlutils.copy
import textwrap

JAP_COL = 3
KAWAII_COL = 6
FLAME_COL = 5
SHEET_COL = 0

DUMP_JAP_COL = 4
DUMP_ENG_COL = 5

#wrap function written by flame
#divides string s into up to num_lines equal length lines at word boundaries 
def wrap(s, num_lines):
    if len(s.split()) <= num_lines:
        #as many words as there are lines, or less
        #Assign one word per line and return
        return s.split()
    max_w = 9999
    while True:
##        print(max_w)
        s1 = textwrap.wrap(s, max_w - 1)    #Test wrap
        w = max(len(x) for x in s1)         #Get length of longest line
        if len(s1) <= num_lines:
            #Will fit in the required number of lines
            if w < max_w:
                #The new maximum width is shorter than before
                max_w = w
                #Then loop
            else:
                #The new maximum width is longer than before
                #Wrap and return using old width value
                return textwrap.wrap(s, max_w)  
        else:
            #Won't fit in the required number of lines
            #Wrap and return using old width value
            return textwrap.wrap(s, max_w)


kitsune_script = xlrd.open_workbook("ch1.xlsx")
script_dump = xlrd.open_workbook("Copy of AnK Scripts v2.xlsx")
script_dump_mod = xlutils.copy.copy(script_dump) #Editable copy

ch1 = kitsune_script.sheet_by_index(2) #Ch1 is third sheet

#diagnostic counters
merged_rows = 0
missing_rows = 0
existing_rows = 0

#loop over every row in chapter 1 in edited Kitsune script
for row in range(ch1.nrows):
    sheet_name = ch1.cell(row, SHEET_COL).value
    #Make sure there is a sheet name and not a Guren translation for this line
    if len(sheet_name) != 5 or ch1.cell(row, 1).value != 0: 
        continue

    sheet = script_dump.sheet_by_index(script_dump.sheet_names().index(sheet_name))
    sheet_mod = script_dump_mod.get_sheet(script_dump.sheet_names().index(sheet_name))

    jap_text = ch1.cell(row, JAP_COL).value #text to match on in script dump

    found_line = False
    merge_row = 0
    while merge_row < sheet.nrows:
        lines_to_fill = 1
        merge_top = merge_row
        jap_match = sheet.cell(merge_row, DUMP_JAP_COL).value
        #merge japanese text in one textbox into a single string to match against
        while sheet.cell(merge_row, 6).value == 'linebreak':
            lines_to_fill += 1
            merge_row += 1
            jap_match += sheet.cell(merge_row, DUMP_JAP_COL).value

        if jap_text == jap_match: 
            found_line = True
            break
        else:
            merge_row += 1

    if not found_line:
        missing_rows += 1
        print "Could not find", row
        continue

    #check that we are not overwriting anything by inserting
    target_blank = True
    for i in range(lines_to_fill):
        if sheet.cell(merge_top+i, DUMP_ENG_COL).value != '':
            target_blank = False

    if not target_blank:
        existing_rows += 1
        continue #Don't overwrite any existing lines

    eng_text = ch1.cell(row, FLAME_COL).value #text to insert
    if eng_text == '':
        eng_text = ch1.cell(row, KAWAII_COL).value
    eng_lines = wrap(eng_text, lines_to_fill)

    for i in range(len(eng_lines)):
        sheet_mod.write(merge_top+i, DUMP_ENG_COL, eng_lines[i])
    merged_rows += 1

script_dump_mod.save("Copy of AnK Scripts v2 Modified.xls")
print "%d merged rows, %d rows with existing translation, %d missing rows" % (merged_rows, existing_rows, missing_rows)


